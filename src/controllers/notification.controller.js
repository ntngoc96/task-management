import httpStatus from "http-status";
import { notification as notificationModel } from "../models/Notification";
import { activity as activityModel } from "../models/Activity";

export async function updateNotificationState(req, res) {
  try {
    const { projectId } = req;
    const { userId } = req.user;
    const { notificationId } = req.params;

    const rowAffected = await notificationModel.update({
      seen: true
    }, {
      where: {
        notificationId,
        projectId,
        userId
      }
    });

    if (!rowAffected[0]) {
      throw { msg: "Falure to update notification state to seen or seen" }
    }

    res.status(httpStatus.OK).json({
      msg: "Change notification to seen successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function getUserNotifications(req, res) {
  try {
    const { userId } = req.user;

    // Associate
    activityModel.hasMany(notificationModel, {
      foreignKey: "activityId"
    });

    notificationModel.belongsTo(activityModel, {
      foreignKey: "activityId"
    });

    const activities = await activityModel.findAll({
      include: {
        model: notificationModel,
        where: {
          userId
        }
      },
      order: [['createdAt', 'DESC']]
    });
    if (activities.length == 0) {
      throw { msg: "No activity found" }
    }

    res.status(httpStatus.OK).json(activities);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}