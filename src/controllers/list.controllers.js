import uuidv4 from 'uuid/v4';
import httpStatus from 'http-status';

import sequelize from '../configs/sequelize.config';

import { list as listModel } from "../models/List";
import { listItem as listItemModel } from "../models/ListItem";
import { activity as activityModel } from "../models/Activity";

export async function addTaskList(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { projectId, userId, taskId } = req.taskMembers;
    const { name } = req.body;
    const listId = uuidv4();

    const list = await listModel.create({
      taskId,
      listId,
      name
    }, { transaction });

    if (!list) {
      throw { msg: "Failure to create task list" }
    }

    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      taskId,
      action: 'add',
      component: 'list',
      message: `${name} to `
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.CREATED).json(list)
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    // error 1452: foreign key constraint
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "Task not found"
      })
    }
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function removeTaskList(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();

    const { listId } = req.params;
    const { isLeader } = req.user;
    const { projectId, userId, taskId } = req.taskMembers;

    let message = {};

    const list = await listModel.findOne({
      where: {
        listId,
        taskId
      }, raw: true
    });

    if (!list) {
      throw { msg: "Task list not found" };
    }

    // If user is leader then permanly delete task
    // Unless soft delete
    if (!isLeader) {
      const [rowAffectedList] = await Promise.all([
        listModel.update({
          isDeleted: true
        }, {
          where: {
            listId,
          }, transaction
        }),
        listItemModel.update({
          isDeleted: true,
        }, {
          where: {
            listId
          }, transaction
        })
      ])


      if (!rowAffectedList[0]) {
        throw { msg: "Failure to remove task list" };
      }

      // store activity
      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        taskId,
        action: 'remove',
        component: 'list',
        message: `${list.name} soft from `
      }, { transaction });

      await transaction.commit();

      message = { msg: "Soft delete task list successfully" };

    } else {
      const rowAffected = await listModel.destroy({
        where: {
          listId,
        }
      });

      if (!rowAffected) {
        throw { msg: "Failure to remove task list" };
      }

      // store activity
      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        taskId,
        action: 'remove',
        component: 'list',
        message: `${list.name} permanently from `
      }, { transaction });

      await transaction.commit();

      message = { msg: "Permanently delete task list successfully" };
    }

    res.status(httpStatus.OK).json(message);
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateTaskList(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { listId } = req.params;
    const { name, position } = req.body;
    const { projectId, userId, taskId } = req.taskMembers;

    const rowAffected = await listModel.update({
      name,
      position
    }, {
      where: {
        listId,
        taskId
      }, transaction
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to update task list or not found" };
    }

    // store activity
    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      taskId,
      action: 'update',
      component: 'list',
      message: `${name} of `
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Update task list successfully"
    });
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}