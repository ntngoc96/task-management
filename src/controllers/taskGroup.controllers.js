import uuidv4 from 'uuid/v4';
import httpStatus from "http-status";
import sequelize from '../configs/sequelize.config';

import { taskGroup as taskGroupModel } from '../models/TaskGroup';
import { task as taskModel } from '../models/Task';
import { activity as activityModel } from '../models/Activity';

export async function createTaskGroup(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { projectId } = req.params;
    const { name } = req.body;
    const { userId } = req.user;
    const taskGroupId = uuidv4();
    const taskGroup = await taskGroupModel.create({
      taskGroupId,
      projectId,
      name,
    }, { transaction });

    // store activity
    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      action: 'add',
      component: 'taskGroup',
      message: `added ${name} to this project`
    }, { transaction });

    if (!taskGroup) {
      throw { msg: "Failure to create task group" }
    }

    await transaction.commit();

    res.status(httpStatus.CREATED).json(taskGroup);
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    // error 1452: foreign key constraint
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "Project not found"
      })
    }

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateTaskGroup(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { userId } = req.user;
    const { projectId } = req;
    const { groupId: taskGroupId } = req.params;
    const { name, position } = req.body;

    const taskGroupName = await taskGroupModel.findOne({
      where: {
        taskGroupId,
        projectId
      },
      attributes: ['name'],
      raw: true
    });

    const rowAffected = await taskGroupModel.update({
      name,
      position
    }, {
      where: {
        taskGroupId,
        projectId
      }, transaction
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to update task group or not found" }
    }
    // store activity
    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      action: 'update',
      component: 'taskGroup',
      message: `updated ${taskGroupName.name} on this project`
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Update task group successfully"
    });
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function removeTaskGroup(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { projectId } = req;
    const { userId, isLeader } = req.user;
    const { groupId: taskGroupId } = req.params;

    const taskGroupName = await taskGroupModel.findOne({
      where: {
        taskGroupId,
        projectId
      },
      attributes: ['name'],
      raw: true
    });

    if (!isLeader) {

      const [rowAffectedTaskGroup] = await Promise.all([
        taskGroupModel.update({
          isDeleted: true
        }, {
          where: {
            taskGroupId,
            projectId
          }, transaction
        }),
        taskModel.update({
          isDeleted: true
        }, {
          where: {
            taskGroupId
          }, transaction
        }),
      ]);

      if (!rowAffectedTaskGroup[0]) {
        throw { msg: "Failure to remove task group or not found" };
      }

      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        action: 'remove',
        component: 'taskGroup',
        message: `removed ${taskGroupName.name} from this project`
      }, { transaction });

      await transaction.commit();

      res.status(httpStatus.OK).json({
        msg: "Soft remove task group successfully"
      });
    } else {
      const rowAffected = await taskGroupModel.destroy({
        where: {
          taskGroupId,
          projectId
        }, transaction
      });

      if (!rowAffected) {
        throw { msg: "Failure to remove task group or not found" };
      }

      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        action: 'remove',
        component: 'taskGroup',
        message: `removed permanently ${taskGroupName.name} from this project`
      }, { transaction });

      await transaction.commit();

      res.status(httpStatus.OK).json({
        msg: "Permanenlty remove task group successfully"
      });
    }
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
} 