import uuidv4 from 'uuid/v4';
import httpStatus from "http-status";

// DB
import sequelize from '../configs/sequelize.config';

// Deepstream instance
import { client } from "../configs/deepstream.config";
// Model
import { taskGroup as taskGroupModel } from "../models/TaskGroup";
import { task as taskModel } from "../models/Task";
import { list as listModel } from "../models/List";
import { listItem as listItemModel } from "../models/ListItem";
import { projectMember as projectMemberModel } from "../models/ProjectMember";
import { projectMemberTask as projectMemberTaskModel } from "../models/ProjectMemberTask";
import { activity as activityModel } from "../models/Activity";
import { notification as notificationModel } from "../models/Notification";

// Helper function
import { getTaskAssociateProject } from '../helpers/model.helpers';

export async function getTaskAndChildren(req, res) {
  try {
    const { taskId } = req.params;
    const { isLeader } = req.user;
    let task;
    taskModel.hasMany(listModel, {
      foreignKey: 'taskId'
    });
    listModel.belongsTo(taskModel, {
      foreignKey: 'taskId'
    });
    listModel.hasMany(listItemModel, {
      foreignKey: 'listId'
    });
    listItemModel.belongsTo(listModel, {
      foreignKey: 'listId'
    });

    if (!isLeader) {
      task = await taskModel.findOne({
        where: {
          taskId,
          isDeleted: false
        },
        include: [{
          model: listModel,
          where: {
            isDeleted: false
          },
          include: [listItemModel]
        }]
      });

    } else {
      task = await taskModel.findOne({
        where: {
          taskId
        },
        include: [{
          model: listModel,
          include: [listItemModel]
        }]
      });
    }

    if (!task) {
      throw { msg: "Task id not found or empty" }
    }

    res.status(httpStatus.OK).json(task);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function createTask(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { groupId: taskGroupId } = req.params;
    const { description, name } = req.body;
    const { userId } = req.user;
    const { projectId } = req;
    const taskId = uuidv4();
    const activityId = uuidv4();
    const notificationId = uuidv4();

    //check task group is child of this project or not
    const taskGroup = await taskGroupModel.findOne({
      where: {
        taskGroupId,
        projectId
      }
    });

    if (!taskGroup) {
      throw { msg: "Task group not in project or not found" }
    }

    const task = await taskModel.create({
      taskId,
      taskGroupId,
      name,
      description
    }, { transaction });

    if (!task) {
      throw { msg: "Failure to create task" }
    }

    //get all project member to store notification
    const projectMembers = await projectMemberModel.findAll({
      where: {
        projectId
      }, raw: true
    });

    for (const element of projectMembers) {
      element.notificationId = uuidv4();
      element.activityId = activityId;
      element.seen = false
    }

    // store activity and notification

    await activityModel.create({
      activityId,
      userId,
      projectId,
      taskId,
      action: 'add',
      component: 'task',
      message: 'to project'
    }, { transaction });

    await notificationModel.bulkCreate(projectMembers, { transaction });


    client.event.emit(`notifications/${projectId}`, {
      activity: `${req.user.userName} added ${name} to project ${projectId}`,
      notificationId
    });

    await transaction.commit();

    res.status(httpStatus.CREATED).json(task);
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    // error 1452: foreign key constraint
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "Task group not found"
      })
    }
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function removeTask(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const activityId = uuidv4();
    const notificationId = uuidv4();
    const { projectId, userId, taskId } = req.taskMembers;
    const { isLeader } = req.user;

    const task = await getTaskAssociateProject(taskId, projectId)
    const { name: taskGroupName } = task.dataValues.taskGroup;

    // check user role
    if (!isLeader) {

      const [rowAffectedTask] = await Promise.all([
        taskModel.update({
          isDeleted: true
        }, {
          where: {
            taskId
          }, transaction
        }),
        listModel.update({
          isDeleted: true
        }, {
          where: {
            taskId
          }, transaction
        })
      ]);

      if (!rowAffectedTask[0]) {
        throw { msg: "Failure to remove task" }
      }
      //get all project member to store notification
      const projectMembers = await projectMemberModel.findAll({
        where: {
          projectId
        }, raw: true
      });

      for (const element of projectMembers) {
        element.notificationId = uuidv4();
        element.activityId = activityId;
        element.seen = false
      }

      // store activity and notification

      awaitactivityModel.create({
        activityId,
        userId,
        projectId,
        taskId,
        action: 'remove',
        component: 'task',
        message: `soft from ${taskGroupName}`
      }, { transaction });

      await notificationModel.bulkCreate(projectMembers, { transaction });

      client.event.emit(`notifications/${projectId}`, {
        activity: `${req.user.userName} removed ${taskId} soft from project ${projectId}`,
        notificationId
      });

      await transaction.commit();

      res.status(httpStatus.OK).json({
        msg: "Soft remove task successfully"
      });
    } else {
      const rowAffected = await taskModel.destroy({
        where: {
          taskId
        }, transaction
      });
      // store activity
      if (!rowAffected) {
        throw { msg: "Failure to remove task list item" }
      }

      // if task was be permanently deleting cascade notification 
      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        action: 'remove',
        component: 'task',
        message: `permanently from ${taskGroupName}`
      }, { transaction });

      client.event.emit(`notifications / ${projectId}`, {
        activity: `${req.user.userName} removed ${taskId} permanently from project ${projectId}`,
      });

      await transaction.commit();

      res.status(httpStatus.OK).json({
        msg: "Permanently remove task successfully"
      });

    }
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function assignMemberToTask(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { taskId } = req.params;
    const { memberId } = req.body;
    const { userId, isLeader } = req.user;
    const { projectId } = req;
    const activityId = uuidv4();
    const notificationId = uuidv4();

    if (!isLeader) {
      throw { msg: "Only the project leader can assign member" }
    }
    const projectMemberTask = await projectMemberTaskModel.create({
      taskId,
      projectId,
      userId: memberId
    }, { transaction });

    if (!projectMemberTask) {
      throw { msg: "Failure to add project member" };
    }

    //get all project member to store notification
    const projectMembers = await projectMemberModel.findAll({
      where: {
        projectId
      }, raw: true
    });

    for (const element of projectMembers) {
      element.notificationId = uuidv4();
      element.activityId = activityId;
      element.seen = false
    }

    // store activity and notification

    await activityModel.create({
      activityId,
      userId,
      projectId,
      taskId,
      memberId,
      action: "assign",
      message: "to "
    }, { transaction });

    await notificationModel.bulkCreate(projectMembers, { transaction });

    client.event.emit(`notifications/${projectId}`, {
      activity: `${req.user.userName} assigned user ${memberId} to task ${taskId}`,
      notificationId
    });

    await transaction.commit();

    res.status(httpStatus.CREATED).json(projectMemberTask);
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    // error 1452: foreign key constraint
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "User was not join into this project"
      })
      // error 1062: primary key duplicate
    } else if (error.parent != undefined && error.parent.errno == 1062) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "User was assigned to this task"
      })
    }
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function removeMemberFromTask(req, res) {
  try {
    const { memberId } = req.query;
    const { isLeader } = req.user;
    const { taskId } = req.params;
    const { projectId } = req;

    if (!isLeader) {
      throw { msg: "User doesn't have permission to remove member" }
    }
    // Don't need to check task in project
    const rowAffected = await projectMemberTaskModel.destroy({
      where: {
        userId: memberId,
        taskId,
        projectId
      }
    })

    if (!rowAffected) {
      throw { msg: "Failure to remove member" }
    }

    res.status(httpStatus.OK).json({
      msg: "Remove member from task successfully"
    })
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateTask(req, res) {
  let transaction;
  try {
    const activityId = uuidv4();
    const notificationId = uuidv4();

    transaction = await sequelize.transaction();
    const { projectId, userId, taskId } = req.taskMembers;
    const { name, description, position } = req.body;
    // function get task include taskgroup and project information
    const task = await getTaskAssociateProject(taskId, projectId);
    const { name: taskGroupName } = task.dataValues.taskGroup;

    const rowAffected = await taskModel.update({
      name,
      description,
      position,
    }, {
      where: {
        taskId
      }, transaction
    });


    if (!rowAffected[0]) {
      throw { msg: "Failure to update task or not found" };
    }

    //get all project member to store notification
    const projectMembers = await projectMemberModel.findAll({
      where: {
        projectId
      }, raw: true
    });

    for (const element of projectMembers) {
      element.notificationId = uuidv4();
      element.activityId = activityId;
      element.seen = false
    }

    // store activity and notification
    await activityModel.create({
      activityId,
      userId,
      projectId,
      taskId,
      action: 'update',
      component: 'task',
      message: `on ${taskGroupName}`
    }, { transaction });

    await notificationModel.bulkCreate(projectMembers, { transaction });

    client.event.emit(`notifications/${projectId}`, {
      activity: `${req.user.userName} updated ${name} on ${taskGroupName}`,
      notificationId
    });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Update task successfully"
    });
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateTaskDueDate(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const activityId = uuidv4();
    const notificationId = uuidv4();
    const { projectId, userId, taskId } = req.taskMembers;
    const { dueDate } = req.body;

    const rowAffected = await taskModel.update({
      dueDate
    }, {
      where: {
        taskId
      }, transaction
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to update due date or not found" };
    }

    //get all project member to store notification
    const projectMembers = await projectMemberModel.findAll({
      where: {
        projectId
      }, raw: true
    });

    for (const element of projectMembers) {
      element.notificationId = uuidv4();
      element.activityId = activityId;
      element.seen = false
    }

    // store activity and notification
    await activityModel.create({
      activityId,
      userId,
      projectId,
      taskId,
      action: 'set',
      component: 'task',
      message: `to be due at ${dueDate}`
    }, { transaction });

    await notificationModel.bulkCreate(projectMembers, { transaction });

    client.event.emit(`notifications/${projectId}`, {
      activity: `${req.user.userName} set ${taskId} to be due date on ${dueDate}`,
      notificationId
    });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Update successfully"
    })
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function getTaskMember(req, res) {
  try {
    const { projectId, taskId } = req.taskMembers;

    const projectMemberTask = await projectMemberTaskModel.findAll({
      where: {
        projectId,
        taskId
      }
    });

    if (projectMemberTask.length == 0) {
      throw { msg: "No member in task" }
    }

    res.status(httpStatus.OK).json(projectMemberTask);

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}