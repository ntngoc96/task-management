import httpStatus from "http-status";
import uuidv4 from 'uuid/v4';
import { uploadPhoto } from "../helpers/cloudinary.helpers";

import { userAccount as userAccountModel } from "../models/User";

import { ses } from "../configs/aws-ses.config";
import { generateParams } from "../helpers/ses.params.helpers";

const VERIFY_TIMEOUT = 600000;

export async function updateInformation(req, res) {
  try {
    const { id: userId } = req.params;
    const { fullName, email, city } = req.body;

    const rowAffected = await userAccountModel.update({
      fullName,
      email,
      city
    }, {
      where: {
        userId
      }
    });

    if (!rowAffected[0]) {
      throw { msg: 'Nothing change or wrong userId' }
    }

    const user = await userAccountModel.findOne({ where: { userId }, attributes: ['userName', 'fullName', 'email', 'city'] });
    if (user) {
      res.status(httpStatus.OK).json(user);
    }
  } catch (error) {
    console.log(error);
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function getUserInformationById(req, res) {
  try {
    const { id: userId } = req.params;
    const user = await userAccountModel.findOne({
      where: { userId },
      attributes: ['userName', 'fullName', 'email', 'city', 'avatar', 'isVerified']
    });
    if (user) {
      res.status(httpStatus.OK).json(user);
    } else {
      throw { msg: "No user found" }
    }
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error);
  }
}

export async function updateAvatar(req, res) {
  try {
    const avatar = req.file;
    if (avatar == undefined) {
      throw { msg: "Avatar field required" }
    }

    const { id: userId } = req.params;

    const imageUrl = await uploadPhoto(avatar, userId, 'Users');

    const rowAffected = await userAccountModel.update({
      avatar: imageUrl
    }, {
      where: {
        userId
      }
    });

    if (!rowAffected[0]) {
      throw { msg: "Avatar wasn't change" }
    }

    res.status(httpStatus.OK).json({
      success: true,
      imageUrl
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error);
  }
}

export async function generateTokenVerifyEmail(req, res) {
  try {
    
    const { id: userId } = req.params;
    const token = uuidv4();
    const user = await userAccountModel.findOne({
      where: {
        userId
      }, attributes: ['email']
    })

    const { email } = user;
    const body = {
      Html: {
        // HTML Format of the email
        Charset: "UTF-8",
        Data:
          `<html>
            <body>
              <h1>Verify email</h1>
              <p style='color:red'>Click link below to verify email</p>
              <a href="http://localhost:8080/api/v1/users/${userId}/verifying?token=${token}">
                http://localhost:8080/api/v1/users/${userId}/verifying?token=${token}
              </a>
              <p>Time 1517831318946</p>
            </body>
          </html>`
      },
      Text: {
        Charset: "UTF-8",
        Data: `http://localhost:8080/api/v1/users/${userId}/verifying?token=${token}`
      }
    }
    const params = generateParams("Confirm verify email", body, email);

    const sendEmail = ses.sendEmail(params).promise();


    let [data, rowAffected] = await Promise.all([
      sendEmail,
      userAccountModel.update({ verifyEmailToken: token, verifyEmailTokenExpire: new Date() }, { where: { userId } })
    ]);
    if (data && rowAffected[0]) {
      res.status(httpStatus.OK).json({
        msg: "Send link verify successfully"
      })
    }
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function confirmVerifyEmail(req, res) {
  try {
    const { id: userId, token } = req.params;
    let user = await userAccountModel.findOne({
      where: {
        userId,
        verifyEmailToken: token
      },
    });

    if (!user) {
      throw { msg: "Failure! User not found or doesn't request to verify email" }
    }
    let now = new Date();

    if (now - user.verifyEmailTokenExpire > VERIFY_TIMEOUT) {
      throw { msg: "Token Expire, the token just alive in 10 minute" }
    } else {
      const rowAffected = await userAccountModel.update({
        isVerified: true
      }, {
        where: {
          userId
        }
      });

      if (!rowAffected[0]) {
        throw { msg: "Email was verified or error appear" }
      }

      res.status(httpStatus.OK).json({
        msg: "Verify email successfully"
      })
    }
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}