import httpStatus from "http-status";
import { activity as activityModel } from "../models/Activity";
export async function getActivitiesOfProject(req, res) {
  try {
    const { projectId } = req;

    const activity = await activityModel.findAll({
      where: {
        projectId
      },
      order: [['createdAt', 'DESC']]
    });

    if (!activity.length) {
      throw { msg: "Failure to get activities or empty" };
    }

    res.status(httpStatus.OK).json(activity);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}