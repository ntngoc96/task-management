import uuidv4 from 'uuid/v4';
import httpStatus from 'http-status';

//DB
import sequelize from '../configs/sequelize.config';

import { listItem as listItemModel } from '../models/ListItem';
import { activity as activityModel } from "../models/Activity";

export async function addListItem(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const listItemId = uuidv4();
    const { listId } = req.params;
    const { name } = req.body;
    const { projectId, userId, taskId } = req.taskMembers;

    const listItem = await listItemModel.create({
      listItemId,
      listId,
      name
    }, { transaction });

    if (!listItem) {
      throw { msg: "Failure to create task list item" };
    }

    // store activity
    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      taskId,
      action: 'add',
      component: 'listItem',
      message: `${name} to `
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.CREATED).json(listItem);
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    // error 1452: foreign key constraint
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "Task list not found"
      })
    }
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function removeListItem(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { listItemId } = req.params;
    const { isLeader } = req.user;
    const { projectId, userId, taskId } = req.taskMembers;
    const { name } = req.childComponent;
    let message = {};

    // If user is leader then permanly delete task
    // Unless soft delete
    if (!isLeader) {
      const rowAffected = await listItemModel.update({
        isDeleted: true
      }, {
        where: {
          listItemId,
        }, transaction
      });

      if (!rowAffected[0]) {
        throw { msg: "Failure to remove task list item" };
      }

      // store activity
      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        taskId,
        action: 'remove',
        component: 'listItem',
        message: `${name} soft from `
      }, { transaction });

      await transaction.commit();

      message = { msg: "Soft delete task list item successfully" };

    } else {
      const rowAffected = await listItemModel.destroy({
        where: {
          listItemId,
        }, transaction
      });

      if (!rowAffected) {
        throw { msg: "Failure to remove task list item" };
      }

      // store activity
      await activityModel.create({
        activityId: uuidv4(),
        userId,
        projectId,
        taskId,
        action: 'remove',
        component: 'listItem',
        message: `${name} permanently from `
      }, { transaction });

      await transaction.commit();

      message = { msg: "Permanently delete task list item successfully" };
    }

    res.status(httpStatus.OK).json(message);


  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateListItem(req, res) {
  try {
    const { listItemId } = req.params;
    const { name, position } = req.body;

    const rowAffected = await listItemModel.update({
      name,
      position,
    }, {
      where: {
        listItemId
      }
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to update task list item" };
    }

    res.status(httpStatus.OK).json({
      msg: "Update task list item successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error);
  }
}

export async function updateListItemState(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { listItemId } = req.params;
    const { projectId,userId,taskId } = req.taskMembers;
    const { name } = req.childComponent;
    let { state } = req.query;
    state = parseInt(state);

    if (state != 0 && state != 1) {
      throw { msg: "State must be 0 or 1" }
    }
    const rowAffected = await listItemModel.update({
      state
    }, {
      where: {
        listItemId
      }, transaction
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to change state" };
    }

    await activityModel.create({
      activityId: uuidv4(),
      projectId,
      userId,
      taskId,
      action: `set`,
      message: `${name} ${state == 0 ? 'uncomplete' : 'complete'} on `
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Update task list item state successfully"
    });
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}