import uuidv4 from 'uuid/v4';
import httpStatus from 'http-status';
import sequelize from '../configs/sequelize.config';

//Send mail config
import { ses } from "../configs/aws-ses.config";

//ds
import { client } from "../configs/deepstream.config";


//Model
import { project as projectModel } from "../models/Project";
import { projectMember as projectMemberModel } from '../models/ProjectMember';
import { activity as activityModel } from '../models/Activity';
import { taskGroup as taskGroupModel, taskGroup } from '../models/TaskGroup';
import { task as taskModel } from '../models/Task';
import { uploadPhoto } from '../helpers/cloudinary.helpers';
import { generateParams } from '../helpers/ses.params.helpers';
import { userAccount as userAccountModel } from '../models/User';
import { notification as notificationModel } from "../models/Notification";

export async function getProject(req, res) {
  try {
    const { projectId } = req;
    const { isLeader } = req.user;
    let project;

    projectModel.hasMany(taskGroupModel, {
      foreignKey: "projectId"
    });
    taskGroupModel.belongsTo(projectModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.hasMany(taskModel, {
      foreignKey: "taskGroupId"
    });
    taskModel.belongsTo(taskGroupModel, {
      foreignKey: "taskGroupId"
    });

    if (!isLeader) {
      project = await projectModel.findOne({
        where: {
          projectId,
        }, include: {
          model: taskGroupModel,
          where: {
            isDeleted: false
          },
          include: {
            model: taskModel,
            where: {
              isDeleted: false
            }
          }
        }
      });

    } else {
      project = await projectModel.findOne({
        where: {
          projectId,
        }, include: {
          model: taskGroupModel,
          include: [taskModel]
        }
      });
    }

    // when user get into this project, they will subscribe notification about this project
    // and then listenning everything on this.
    client.event.subscribe(`notifications/${projectId}`, data => {
      console.log('receive data: ', data);

    })

    if (!project) {
      throw { msg: "Project not found" };
    };


    res.status(httpStatus.OK).json(project);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function createProject(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { name, description } = req.body;
    const { userId } = req.user;
    const projectId = uuidv4();


    const [project, projectLeader] = await Promise.all([
      projectModel.create({
        projectId,
        projectLeaderId: userId,
        name,
        description
      }, { transaction }),
      projectMemberModel.create({
        projectId,
        userId
      }, { transaction })
    ]);

    if (!project || !projectLeader) {
      throw { msg: "Failure to create project" }
    }

    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      action: 'add',
      component: 'project',
      message: 'created this project'
    }, { transaction });

    await transaction.commit();


    res.status(httpStatus.CREATED).json({
      project,
      projectLeader
    });
  } catch (error) {
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "User not found"
      })
    }

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateProject(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { projectId } = req.params;
    const { userId, isLeader } = req.user;
    const { name, description } = req.body;


    if (!isLeader) {
      throw { msg: "User doesn't have permission to update project" };
    }

    const rowAffected = await projectModel.update({
      name,
      description
    }, {
      where: {
        projectId
      }, transaction
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to update" };
    }

    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      action: 'update',
      component: 'project',
      message: 'updated information of project'
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Update project successfully"
    });
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function addMemberToProject(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { projectId } = req;
    const { memberId } = req.query;
    const { userId, isLeader } = req.user;
    const notificationId = uuidv4();
    const activityId = uuidv4();

    if (!isLeader) {
      throw { msg: "User doesn't have permission to assign member" };
    }

    //Get user and project information to send email
    const [project, user] = await Promise.all([
      projectModel.findOne({
        where: { projectId },
        attributes: ['name']
      }),
      userAccountModel.findOne({
        where: { userId: memberId },
        attributes: ['email']
      })
    ])

    const projectMember = await projectMemberModel.create({
      userId: memberId,
      projectId
    }, { transaction });

    if (!projectMember) {
      throw { msg: "Failure to add project member" };
    }

    const body = {
      Html: {
        // HTML Format of the email
        Charset: "UTF-8",
        Data:
          `<html>
            <body>
              <h1>${project.name}</h1>
              <p style='color:red'>You was invited to ${project.name} project</p>
              
              <p>Time 1517831318946</p>
            </body>
          </html>`
      },
      Text: {
        Charset: "UTF-8",
        Data: `You was invited to ${project.name}`
      }
    };
    const params = generateParams(`You are invited to project ${project.name}`, body, user.email);

    const sendEmail = ses.sendEmail(params).promise();


    let [data] = await Promise.all([
      sendEmail,
      activityModel.create({
        activityId,
        userId,
        projectId,
        memberId,
        action: 'add',
        component: 'project',
        message: 'to this project'
      }, { transaction }),
    ]);

    await notificationModel.create({
      notificationId,
      projectId,
      userId,
      activityId,
      seen: false
    }, { transaction })

    console.log(data);
    console.log(user.email);

    if (!data) {
      throw { msg: "Failure to send link notify invited" }
    }

    // Send notification to all project members
    client.event.emit(`notifications/${projectId}`, {
      activity: `${req.user.userName} added ${memberId} to project ${projectId}`,
      notificationId
    });
    // Send notification to user that was invited
    client.event.emit(`notifications/${memberId}`, {
      activity: `${req.user.userName} added you to project ${projectId}`,
      notificationId
    });

    await transaction.commit();
    res.status(httpStatus.CREATED).json({
      projectMember,
      msg: "Send link notify successfully"
    })
  } catch (error) {
    console.log(error);

    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    // error 1452: foreign key constraint
    if (error.parent != undefined && error.parent.errno == 1452) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "User not found"
      })
      // error 1062: primary key duplicate
    } else if (error.parent != undefined && error.parent.errno == 1062) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: "User was joined on this project"
      })
    }
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function removeMemberFromProject(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { projectId } = req;
    const { userId, isLeader } = req.user;
    const { memberId } = req.query;

    if (!isLeader) {
      throw { msg: "User doesn't have permission to remove member" };
    }

    const rowAffected = await projectMemberModel.destroy({
      where: {
        userId: memberId,
        projectId
      }, transaction
    });

    if (!rowAffected) {
      throw { msg: "Cannot remove this member or not found" };
    }

    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      memberId,
      action: 'remove',
      component: 'project',
      message: 'from this project'
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Remove member successfully"
    })
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function updateAvatar(req, res) {
  let transaction;
  try {

    if (!req.file) {
      throw { msg: "Avatar field is required" };
    }
    transaction = await sequelize.transaction();
    const avatar = req.file;
    const { userId, isLeader } = req.user;
    const { projectId } = req;

    if (!isLeader) {
      throw { msg: "User doesn't have permission to update project avatar" };
    }

    const imageUrl = await uploadPhoto(avatar, projectId, "ProjectAvatars");

    const rowAffected = await projectModel.update({
      imageUrl
    }, {
      where: {
        projectId,
      }, transaction
    });

    if (!rowAffected[0]) {
      throw { msg: "Failure to update or project not found" };
    }

    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      action: 'update',
      component: 'project',
      message: "updated project's avatar"
    }, { transaction });

    await transaction.commit();


    res.status(httpStatus.OK).json({
      msg: "Update project avatar successfully",
      imageUrl
    })
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function memberLeaveProject(req, res) {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const { userId, isLeader } = req.user;
    const { projectId } = req;
    if (isLeader) {
      throw { msg: "Project owner cannot leave the project" };
    }

    const rowAffected = await projectMemberModel.destroy({
      where: {
        userId,
        projectId
      }, transaction
    });

    // member may be has or hasn't tasks
    if (!rowAffected) {
      throw { msg: "Failure to leave the project" };
    }

    await activityModel.create({
      activityId: uuidv4(),
      userId,
      projectId,
      action: 'leave',
      component: 'project',
      message: 'left this project'
    }, { transaction });

    await transaction.commit();

    res.status(httpStatus.OK).json({
      msg: "Leaving the project successfully"
    });
  } catch (error) {
    console.log(error);
    // Rollback transaction only if the transaction object is defined
    if (transaction) await transaction.rollback();
    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function getProjectMembers(req, res) {
  try {
    const { projectId } = req;
    const projectMember = await projectMemberModel.findAll({
      where: {
        projectId
      },
      attributes: ["userId"]
    });

    if (!projectMember) {
      throw { msg: "Project doesn't have member" }
    }

    res.status(httpStatus.OK).json(projectMember);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function deleteProject(req, res) {
  try {
    const { userId, isLeader } = req.user;
    const { projectId } = req;

    if (!isLeader) {
      throw { msg: "User doesn't have permission to delete project" }
    }

    const rowAffected = await projectModel.destroy({
      where: {
        projectId,
        projectLeaderId: userId
      }
    });

    if (!rowAffected) {
      throw { msg: "Falure to delete project" }
    }

    res.status(httpStatus.OK).json({
      msg: "Permanenlty delete project successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function getAllUserProjects(req, res) {
  try {
    const { userId } = req.user;
    // associate
    projectModel.hasMany(projectMemberModel, {
      foreignKey: 'projectId'
    });

    projectMemberModel.belongsTo(projectModel, {
      foreignKey: 'projectId'
    })
    const projects = await projectMemberModel.findAll({
      where: {
        userId
      }, include: [projectModel]
    });

    if (projects.length == 0) {
      throw { msg: "No project found" }
    }

    res.status(httpStatus.OK).json(projects);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}