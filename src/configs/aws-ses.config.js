import AWS from 'aws-sdk';

// AWS
AWS.config.update({
  accessKeyId: process.env.AWS_SES_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SES_SECRET_ACCESS_KEY,
  region: process.env.AWS_SES_REGION
});

export const ses = new AWS.SES({ apiVersion: "2010-12-01" });

export const EMAIL_SRC = "ntngocwd96@gmail.com";