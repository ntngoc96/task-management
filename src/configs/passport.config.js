import passport from 'passport';
import LocalStrategy from 'passport-local';
import uuidv4 from 'uuid/v4';
import bcrypt from 'bcrypt';

import { userAccount as userAccountModel } from "../models/User";

// call when authenticate successfully then store user into session
passport.serializeUser((user, done) => {
  done(null, user.userName);
});

// get data user from session then pass into req.user
passport.deserializeUser(async (userName, done) => {
  try {
    const userAccount = await userAccountModel.findOne({
      where: {
        userName
      },
      raw: true
    });

    if (userAccount) {
      done(null, userAccount);
    } else {
      throw { msg: 'user not found' }
    }
  } catch (error) {
    done(error)
  }
});

// passport signup
passport.use('local-signup', new LocalStrategy({
  // default local strategy using username and password, we can change it if needed  
  usernameField: 'userName',
  // passwordField: 'password',
  passReqToCallback: true
}, async (req, userName, password, done) => {
  try {
    let userAccount = await userAccountModel.findOne({
      where: {
        userName
      }
    });

    if (userAccount) {
      return done({ 'msg': 'username exist' }, false);
    } else {
      let { email } = req.body;
      password = await bcrypt.hash(password, parseInt(process.env.SALT_ROUND));
      const user = {
        userId: uuidv4(),
        userName,
        password,
        email,
      }

      userAccount = await userAccountModel.create(user, { raw: true });

      if (userAccount) {
        done(null, userAccount);
      }
    }
  } catch (error) {
    console.log(error);

    return done(error)
  }
})
);

passport.use('local-signin', new LocalStrategy({
  usernameField: 'userName',
  passReqToCallback: true
}, async (req, userName, password, done) => {
  try {
    let userAccount = await userAccountModel.findOne({
      where: {
        userName
      },
      attributes: ['userId', 'userName','password', 'fullName', 'email', 'isVerified'],
      raw: true,
    });

    if (!userAccount) {
      done({ msg: "Username not exist" }, false);
    } else {
      let matchPassword = await bcrypt.compare(password, userAccount.password);
      if (!matchPassword) {
        done({ msg: "Wrong password" }, false);
      } else {
        delete userAccount.password;
        done(null, userAccount);
      }
    }
  } catch (error) {
    console.log(error);

    done(error)
  }
})
)