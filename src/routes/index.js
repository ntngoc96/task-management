import express from 'express';

const router = express.Router();

import { isLoggedIn } from "../middlewares/passport.middlewares";

import { authRoute } from "./auth/auth.routes";
import { projectRoute } from "./api/project.routes";
import { userRoute } from './api/user.routes';
import { taskGroupRoute } from './api/taskGroup.routes';
import { taskRoute } from './api/task.routes';
import { listRoute } from './api/list.routes';
import { listItemRoute } from './api/listItem.routes';
import { activityRoute } from './api/activity.routes';
import { notificationRoute } from './api/notification.routes';

/* GET home page. */
router.get('/', function (req, res, next) {
  console.log(process.env.PORT);

  res.render('index', { title: 'Express' });
});

router.use('/auth/v1', authRoute);
router.use('/api/v1/projects', isLoggedIn, projectRoute);
router.use('/api/v1/users', isLoggedIn, userRoute);
router.use('/api/v1/task-groups', isLoggedIn, taskGroupRoute);
router.use('/api/v1/tasks', isLoggedIn, taskRoute);
router.use('/api/v1/lists', isLoggedIn, listRoute);
router.use('/api/v1/list-items',isLoggedIn, listItemRoute);
router.use('/api/v1/activities',isLoggedIn, activityRoute);
router.use('/api/v1/notifications',isLoggedIn, notificationRoute);


module.exports = router;
