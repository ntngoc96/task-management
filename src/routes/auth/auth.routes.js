import express from 'express';
import passport from 'passport';
import httpStatus from 'http-status';

import { client } from "../../configs/deepstream.config";

const router = express.Router();

import { validateSignUp, validateSignIn } from '../../validations/auth.validations';
import { handleValidationError } from '../../middlewares/validation.middlewares';
import { DeepstreamClient } from '@deepstream/client';

/**
* @api {POST} /auth/v1/signin
* @apiDescription User sign in
* @apiGroup Auth
* @apiPermission Public
*
* @apiParam {String} userName
* @apiParam {String} password
*
* @apiSuccess (Created 201) {Boolean} success - true
* @apiSuccess (Created 201) {Object} user - User infomation
*
* @apiError(Unauthorized 401) {Boolean} success - true 
* @apiError(Unauthorized 401) {Object} message - Error message 
*
* @apiError(Unauthorized 400) {Array} message - Validate error 
*/
router.route('/signin')
  .post(validateSignIn, handleValidationError, passport.authenticate('local-signin', { failWithError: true }),
    function (req, res, next) {
      // Handle success
      return res.status(httpStatus.OK).json({ success: true, user: req.user });
    },
    function (err, req, res, next) {
      // Handle error
      return res.status(httpStatus.UNAUTHORIZED).send({ success: false, message: err })
    });


/**
* @api {POST} /auth/v1/signup
* @apiDescription User sign up
* @apiGroup Auth
* @apiPermission Public
*
* @apiParam {String} userName
* @apiParam {String} password
* @apiParam {String} email
*
* @apiSuccess (Created 201) {Boolean} success - true
* @apiSuccess (Created 201) {Object} user - User infomation
*
* @apiError(Unauthorized 401) {Boolean} success - true 
* @apiError(Unauthorized 401) {Object} message - Error message 
*
* @apiError(Unauthorized 400) {Array} message - Validate error 
*/

router.route('/signup')
  .post(validateSignUp, handleValidationError, passport.authenticate('local-signup', { failWithError: true }),
    function (req, res, next) {
      // Handle success
      client.login({ username: req.user.userName, password: req.user.password }, (success, data) => {
        if (success) {
          // start application
          client.event.subscribe('notifications', data => {
            console.log(data);

          })

          // client.getConnectionState() will now return 'OPEN'
        } else {
          // extra data can be optionaly sent from deepstream for
          // both successful and unsuccesful logins
          console.log(data)

          // client.getConnectionState() will now return
          // 'AWAITING_AUTHENTICATION' or 'CLOSED'
          // if the maximum number of authentication
          // attempts has been exceeded.
        }
      })
      return res.status(httpStatus.CREATED).json({ success: true, user: req.user });
    },
    function (err, req, res, next) {
      // Handle error
      return res.status(httpStatus.UNAUTHORIZED).send({ success: false, message: err })
    });


export { router as authRoute }