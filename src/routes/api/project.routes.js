import express from 'express';
import { createTaskGroup } from '../../controllers/taskGroup.controllers';
import { createProject, addMemberToProject, removeMemberFromProject, updateProject, getProject, updateAvatar, memberLeaveProject, getProjectMembers, deleteProject, getAllUserProjects } from '../../controllers/project.controllers';
import { validateProjectCreate, validateProjectUpdate } from '../../validations/project.validations';
import { handleValidationError } from '../../middlewares/validation.middlewares';
import { validateTaskGroupCreate } from '../../validations/taskGroup.validations';
import { isMemberInTheProject, isProjectLeader } from '../../middlewares/role.middlewares';
import { multerUpload } from '../../configs/multer.config';
const router = express.Router();


/**
* @api {GET} /api/v1/projects
* @apiDescription Get all user projects
* @apiGroup User
* @apiPermission Private
*
* @apiParam {String} id - userId
*
* @apiSuccess (OK 200) {Array} project - project infomation
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - No project found
* 
*/
/**
* @api {POST} /api/v1/projects
* @apiDescription Create new projects
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} name
* @apiParam {String} description
*
* @apiSuccess (Created 201) {Object} project 
* @apiSuccess (Created 201) {Object} projectOwner
*
* @apiError(Unauthorized 401) {String} message - Error message
* 
* @apiError(Bad Request 400) {String} msg - Failure to create project
*/
router.route('/')
  .get(getAllUserProjects)
  .post(validateProjectCreate,
    handleValidationError,
    createProject);


/**
* @api {GET} /api/v1/projects/:projectId
* @apiDescription Get project information
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} projectId 
* @apiSuccess (OK 200) {String} projectLeaderId 
* @apiSuccess (OK 200) {String} name 
* @apiSuccess (OK 200) {String} description 
* @apiSuccess (OK 200) {Boolean} recent 
* @apiSuccess (OK 200) {Boolean} isDeleted 
* @apiSuccess (OK 200) {String} imageUrl 
* @apiSuccess (OK 200) {Array} taskGroup 
* @apiSuccess (OK 200) {Array} task - inside taskgroup 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project

* @apiError(Bad Request 400) {String} msg - Project not found
*
*/


/**
* @api {PATCH} /api/v1/projects/:projectId
* @apiDescription Update specify project
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiParam {String} name
* @apiParam {String} description
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
*
* @apiError(Bad Request 400) {Array} msg - Validate error
* 
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/
router.route('/:projectId')
  .get(isMemberInTheProject,
    isProjectLeader,
    getProject)
  .patch(isMemberInTheProject,
    isProjectLeader,
    validateProjectUpdate,
    handleValidationError,
    updateProject)
  .delete(
    isMemberInTheProject,
    isProjectLeader,
    deleteProject
  )


/**
* @api {GET} /api/v1/projects/:projectId/members
* @apiDescription Get members of project
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiSuccess (Created 201) {String} userId
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not member of project
*
*/

/**
* @api {POST} /api/v1/projects/:projectId/members?memberId=:memberId
* @apiDescription Add member to exist project
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
* @apiParam {String} memberId
*
* @apiSuccess (Created 201) {String} projectId 
* @apiSuccess (Created 201) {String} userId - member id added
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission to add member
* 
* @apiError(Bad Request 400) {String} msg - User not found
*
* @apiError(Bad Request 400) {String} msg - User was joinned on this project
*/


/**
* @api {DELETE} /api/v1/projects/:projectId/members?memberId=:memberId
* @apiDescription remove member from project
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
* @apiParam {String} memberId
*
* @apiSuccess (OK 200) {String} msg - Remove successfully message
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission to add member
* 
* @apiError(Bad Request 400) {String} msg - Cannot remove or user member not found
*/

router.route('/:projectId/members')
  .get(isMemberInTheProject,
    getProjectMembers
  )
  .post(isMemberInTheProject,
    isProjectLeader,
    addMemberToProject)
  .delete(isMemberInTheProject,
    isProjectLeader,
    removeMemberFromProject)


/**
* @api {DELETE} /api/v1/projects/:projectId/leaving
* @apiDescription Member leave the project
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Leaving the project successfully message
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {String} msg - Project owner cannot leave the project
* 
* @apiError(Bad Request 400) {String} msg - Failure to leave the project
*/

router.route('/:projectId/leaving')
  .delete(isMemberInTheProject,
    isProjectLeader,
    memberLeaveProject
  )

/**
* @api {PUT} /api/v1/projects/:projectId/avatar
* @apiDescription Update project avatar
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiParam {String} avatar
*
* @apiSuccess (OK 200) {String} msg - Update successfully message
* @apiSuccess (OK 200) {String} avatarUrl 
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission to update avatar
* 
* @apiError(Bad Request 400) {String} msg - Validate error
*
* @apiError(Bad Request 400) {String} msg - Update error message
*/
router.route('/:projectId/avatar')
  .put(isMemberInTheProject,
    isProjectLeader,
    multerUpload.single('avatar'),
    updateAvatar);


/**
* @api {POST} /api/v1/projects/:projectId/task-groups
* @apiDescription Create new task group
* @apiGroup Project
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiParam {String} name
* @apiParam {String} userId
*
* @apiSuccess (Created 201) {String} taskGroupId
* @apiSuccess (Created 201) {String} projectId 
* @apiSuccess (Created 201) {String} name
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
*
* @apiError(Bad Request 400) {Array} message - Validate error
* 
* @apiError(Bad Request 400) {String} msg - Project not found
*/
router.route('/:projectId/task-groups')
  .post(isMemberInTheProject,
    validateTaskGroupCreate,
    handleValidationError,
    createTaskGroup);



export { router as projectRoute }