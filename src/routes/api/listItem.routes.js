import express from 'express';
import {
  removeListItem,
  updateListItem,
  updateListItemState
} from '../../controllers/listItem.controllers';
import { handleValidationError } from '../../middlewares/validation.middlewares';
import {
  isMemberInTheProject,
  isProjectLeader,
  isListItemOfTheProject,
  handleCheckComponentOfTheProject,
  canManipulateTask,
  isListItemOfTask
} from '../../middlewares/role.middlewares';
import { validateListItemUpdate } from '../../validations/listItem.validations';

const router = express.Router();


/**
* @api {PATCH} /api/v1/list-items/:listItemId
* @apiDescription Update task list item
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} listItemId
*
* @apiParam {String} name
* @apiParam {String} position
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
* 
* @apiError(Bad Request 400) {String} msg - Validate error
*
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/

/**
* @api {DELETE} /api/v1/list-items/:listItemId
* @apiDescription Remove specify task list item
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} listItemId
* @apiParam {String} projectId
* @apiParam {String} taskId
*
* @apiSuccess (OK 200) {String} msg - Remove successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
* 
* @apiError(Bad Request 400) {String} msg - Failure to remove or not found
*/
router.route('/:listItemId')
  .patch(
    canManipulateTask,
    isListItemOfTask,
    validateListItemUpdate,
    handleValidationError,
    updateListItem)
  .delete(
    canManipulateTask,
    isListItemOfTask,
    isProjectLeader,
    removeListItem);


/**
* @api {PATCH} /api/v1/list-items/:listItemId/state?state=:state&projectId=:projectId
* @apiDescription Update task list item state
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} listItemId
*
* @apiParam {String} state
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
* 
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/
router.route('/:listItemId/state')
  .patch(
    canManipulateTask,
    isListItemOfTask,
    updateListItemState)

export { router as listItemRoute }
