import express from 'express';
import { addListItem } from '../../controllers/listItem.controllers';
import { validateListItemCreate } from '../../validations/listItem.validations';
import { handleValidationError } from '../../middlewares/validation.middlewares';
import { removeTaskList, updateTaskList } from '../../controllers/list.controllers';
import { isProjectLeader, canManipulateTask, isListOfTask } from '../../middlewares/role.middlewares';
import { validateTaskListUpdate } from '../../validations/list.validations';

const router = express.Router();

/**
* @api {POST} /api/v1/lists/:listId/list-items
* @apiDescription Add task list item
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} listItemId
*
* @apiParam {String} name
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} listId 
* @apiSuccess (OK 200) {String} listItemId 
* @apiSuccess (OK 200) {String} name 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
* 
* @apiError(Bad Request 400) {String} msg - Validate error

* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/
router.route('/:listId/list-items')
  .post(
    canManipulateTask,
    isListOfTask,
    validateListItemCreate,
    handleValidationError,
    addListItem)


/**
* @api {PATCH} /api/v1/lists/:listId
* @apiDescription Update specify task list
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} listId
*
* @apiParam {String} name
* @apiParam {String} position
* @apiParam {String} projectId
* @apiParam {String} taskId
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {Array} msg - Validate error
* 
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/


/**
* @api {DELETE} /api/v1/lists/:listId
* @apiDescription Remove specify task list
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} listId
* @apiParam {String} projectId
* @apiParam {String} taskId
*
* @apiSuccess (OK 200) {String} msg - Remove successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
* 
* @apiError(Bad Request 400) {String} msg - Failure to remove or not found
*/
router.route('/:listId')
  .patch(
    canManipulateTask,
    isListOfTask,
    validateTaskListUpdate,
    handleValidationError,
    updateTaskList)
  .delete(
    canManipulateTask,
    isProjectLeader,
    removeTaskList)



export { router as listRoute }