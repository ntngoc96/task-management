import express from 'express';
import { updateNotificationState, getUserNotifications } from '../../controllers/notification.controller';
import { isMemberInTheProject } from '../../middlewares/role.middlewares';

const router = express.Router();
/**
* @api {GET} /api/v1/notifications/:notificationId
* @apiDescription Update notification state
* @apiGroup Notification
* @apiPermission Private
*
* @apiParam {String} projectId
* @apiParam {String} notificationId
*
* @apiSuccess (Created 201) {String} msg - Change state message  
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - Failure to change or that is not user notification
* 
*/
router.route('/:notificationId')
  .patch(
    isMemberInTheProject,
    updateNotificationState)

/**
* @api {GET} /api/v1/notifications
* @apiDescription Get notifications of user
* @apiGroup Notification
* @apiPermission Private
*
* @apiSuccess (OK 200) {Array} notification 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - Failure to get or empty
* 
*/
router.route('/')
  .get(
    getUserNotifications
  )

export { router as notificationRoute }