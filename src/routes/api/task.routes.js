import express from 'express';
import { addTaskList } from '../../controllers/list.controllers';
import { validateTaskListCreate } from '../../validations/list.validations';
import { handleValidationError } from '../../middlewares/validation.middlewares';
import { assignMemberToTask, removeTask, updateTask, updateTaskDueDate, getTaskMember, getTaskAndChildren, removeMemberFromTask } from '../../controllers/task.controllers';
import { isMemberInTheProject, isProjectLeader, isTaskOfTheProject, handleCheckComponentOfTheProject, canManipulateTask } from '../../middlewares/role.middlewares';
import { validateMemberTaskAssign, validateTaskUpdate, validateTaskUpdateDueDate } from '../../validations/task.validations';


const router = express.Router();

/**
* @api {GET} /api/v1/tasks/:taskId/lists
* @apiDescription Get task and children
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} projectId
* @apiParam {String} name
*
* @apiSuccess (Created 201) {String} taskId 
* @apiSuccess (Created 201) {String} taskGroupId 
* @apiSuccess (Created 201) {String} name
* @apiSuccess (Created 201) {String} description
* @apiSuccess (Created 201) {String} position
* @apiSuccess (Created 201) {String} dueDate
* @apiSuccess (Created 201) {String} isDeleted
* @apiSuccess (Created 201) {Object[]} lists
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
* 
* @apiError(Bad Request 400) {Array} message - Validate error
*
* @apiError(Bad Request 400) {String} msg - Task not found
*/

/**
* @api {POST} /api/v1/tasks/:taskId/lists
* @apiDescription Create new task list
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} projectId
* @apiParam {String} name
*
* @apiSuccess (Created 201) {String} taskId 
* @apiSuccess (Created 201) {String} listId 
* @apiSuccess (Created 201) {String} name
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
* 
* @apiError(Bad Request 400) {Array} message - Validate error
*
* @apiError(Bad Request 400) {String} msg - Task group not found
*/
router.route('/:taskId/lists')
  .get(
    canManipulateTask,
    isProjectLeader,
    getTaskAndChildren)
  .post(
    canManipulateTask,
    validateTaskListCreate,
    handleValidationError,
    addTaskList);

/**
* @api {GET} /api/v1/tasks/:taskId/members
* @apiDescription Get members of task
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} projectId
*
* @apiSuccess (Created 201) {String} taskId 
* @apiSuccess (Created 201) {String} projectId 
* @apiSuccess (Created 201) {String} userId
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not member of project
*
* @apiError(Bad Request 400) {String} msg - User is not assign to task
* 
* @apiError(Bad Request 400) {Array} message - Validate error
*
*/

/**
* @api {POST} /api/v1/tasks/:taskId/members
* @apiDescription Assign member to task
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} projectId
* @apiParam {String} memberId - userId in projectMember
*
* @apiSuccess (Created 201) {String} taskId 
* @apiSuccess (Created 201) {String} projectId 
* @apiSuccess (Created 201) {String} userId - memberId added
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
* 
* @apiError(Bad Request 400) {Array} message - Validate error
*
* @apiError(Bad Request 400) {String} msg - User was assigned
*
* @apiError(Bad Request 400) {String} msg - User wasn't join into this project
*/
router.route('/:taskId/members')
  .get(
    canManipulateTask,
    getTaskMember
  )
  .post(isTaskOfTheProject,
    handleCheckComponentOfTheProject,
    isMemberInTheProject,
    isProjectLeader,
    validateMemberTaskAssign,
    handleValidationError,
    assignMemberToTask)
  .delete(
    isMemberInTheProject,
    isProjectLeader,
    removeMemberFromTask);


/**
* @api {PATCH} /api/v1/tasks/:taskId
* @apiDescription Update specify task
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} name
* @apiParam {String} description
* @apiParam {String} position
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {Array} msg - Validate error
* 
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/


/**
* @api {DELETE} /api/v1/tasks/:taskId?projectId=:projectId
* @apiDescription Remove task
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Remove task successfully 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
* 
* @apiError(Bad Request 400) {String} msg - Task not found
*/
router.route('/:taskId')
  .patch(
    canManipulateTask,
    validateTaskUpdate,
    handleValidationError,
    updateTask)
  .delete(
    canManipulateTask,
    isProjectLeader,
    removeTask)

/**
* @api {PATCH} /api/v1/tasks/:taskId/due-date
* @apiDescription Update task due date
* @apiGroup Task
* @apiPermission Private
*
* @apiParam {String} taskId
*
* @apiParam {String} dueDate - YYYY-MM-DD
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {Array} msg - Validate error
* 
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/

router.route('/:taskId/due-date')
  .patch(
    canManipulateTask,
    validateTaskUpdateDueDate,
    handleValidationError,
    updateTaskDueDate)




export { router as taskRoute }