import express from 'express';
import { validateTaskCreate } from '../../validations/task.validations';
import { handleValidationError } from '../../middlewares/validation.middlewares';
import { createTask } from '../../controllers/task.controllers';
import { isMemberInTheProject, isProjectLeader } from '../../middlewares/role.middlewares';
import { validateTaskGroupUpdate } from '../../validations/taskGroup.validations';
import { updateTaskGroup, removeTaskGroup } from '../../controllers/taskGroup.controllers';

const router = express.Router();


/**
* @api {PATCH} /api/v1/task-groups/:groupId
* @apiDescription Update specify task group
* @apiGroup TaskGroup
* @apiPermission Private
*
* @apiParam {String} groupId - Task group id
*
* @apiParam {String} name
* @apiParam {String} position
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Update successfully message 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
*
* @apiError(Bad Request 400) {Array} msg - Validate error
* 
* @apiError(Bad Request 400) {String} msg - Failure to update or not found
*/

/**
* @api {DELETE} /api/v1/task-groups/:groupId?projectId=:projectId
* @apiDescription Remove specify task group
* @apiGroup TaskGroup
* @apiPermission Private
*
* @apiParam {String} groupId - Task group id
*
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {String} msg - Remove task group successfully 
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User is not a member of the project
* 
* @apiError(Bad Request 400) {String} msg - Failure to update task group or not found
*/

router.route('/:groupId')
  .patch(isMemberInTheProject,
    validateTaskGroupUpdate,
    handleValidationError,
    updateTaskGroup)
  .delete(isMemberInTheProject,
    isProjectLeader,
    removeTaskGroup)


/**
* @api {POST} /api/v1/task-groups/:groupId/tasks
* @apiDescription Create new tasks
* @apiGroup TaskGroup
* @apiPermission Private
*
* @apiParam {String} groupId
*
* @apiParam {String} name
* @apiParam {String} description
* @apiParam {String} projectId
*
* @apiSuccess (Created 201) {String} taskId 
* @apiSuccess (Created 201) {String} taskGroupId
* @apiSuccess (Created 201) {String} name
* @apiSuccess (Created 201) {String} description
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - User doesn't have permission
* 
* @apiError(Bad Request 400) {Array} message - Validate error
*
* @apiError(Bad Request 400) {String} msg - Task group not found
*
*/

router.route('/:groupId/tasks')
  .post(isMemberInTheProject,
    validateTaskCreate,
    handleValidationError,
    createTask)

export { router as taskGroupRoute }