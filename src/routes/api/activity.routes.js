import express from 'express';
import { isMemberInTheProject } from '../../middlewares/role.middlewares';
import { getActivitiesOfProject } from '../../controllers/activity.controllers';

const router = express.Router();
/**
* @api {GET} /api/v1/activities/:projectId
* @apiDescription Get activites of project
* @apiGroup Activity
* @apiPermission Private
*
* @apiParam {String} projectId
*
* @apiSuccess (OK 200) {Array} activities
*
* @apiError(Unauthorized 401) {String} msg - Error message
*
* @apiError(Bad Request 400) {String} msg - Failure to get or empty
* 
*/
router.route('/:projectId')
  .get(
    isMemberInTheProject,
    getActivitiesOfProject)

export { router as activityRoute }