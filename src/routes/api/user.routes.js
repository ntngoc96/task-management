import express from 'express';
import { updateInformation, getUserInformationById, updateAvatar, generateTokenVerifyEmail, confirmVerifyEmail } from '../../controllers/user.controllers';
import { validateUserUpdateInfo } from '../../validations/user.validations';
import { handleValidationError } from '../../middlewares/validation.middlewares';

// Multer config
import { multerUpload } from '../../configs/multer.config';
import { getAllUserProjects } from '../../controllers/project.controllers';

const router = express.Router();

/**
* @api {GET} /api/v1/users/:id
* @apiDescription Get user information
* @apiGroup User
* @apiPermission Private
*
* @apiParam {String} id - userId
*
* @apiSuccess (OK 200) {Object} user - User infomation
*
* @apiError(Unauthorized 401) {Object} message - Error message
*
* @apiError(Bad Request 400) {String} msg - No user found
* 
*/


/**
* @api {PATCH} /api/v1/users/:id
* @apiDescription User update information
* @apiGroup User
* @apiPermission Private
*
* @apiParam {String} fullName
* @apiParam {String} password
* @apiParam {String} email - optional if email not verify
*
* @apiSuccess (OK 200) {Object} user - User infomation
*
* @apiError(Unauthorized 401) {Object} message - Error message
* 
* @apiError(Bad Request 400) {String} msg - Nothing change in db
*
* @apiError(Bad Request 400) {Array} msg - Error validate message 
*/
router.route('/:id')
  .get(getUserInformationById)
  .patch(validateUserUpdateInfo,
    handleValidationError,
    updateInformation);


/**
* @api {PATCH} /api/v1/users/:id/avatar
* @apiDescription User update avatar
* @apiGroup User
* @apiPermission Private
*
* @apiParam {String} id - userId
*
* @apiParam {Binary} avatar
*
* @apiSuccess (OK 200) {Object} success - true
* @apiSuccess (OK 200) {Object} imageUrl - Avatar url
*
* @apiError(Unauthorized 401) {Object} message - Error message
* 
* @apiError(Bad Request 400) {String} msg - Avatar not change
*
* @apiError(Bad Request 400) {String} msg - Error validate message 
*/
router.route('/:id/avatar')
  .patch(multerUpload.single('avatar'),
    updateAvatar);

/**
* @api {PATCH} /api/v1/users/:id/verifying
* @apiDescription System generate token for user verify email
* @apiGroup User
* @apiPermission Private
*
* @apiParam {String} id - userId
*
* @apiParam {String} email
*
* @apiSuccess (OK 200) {String} msg - "Send link verify successfully"
*
* @apiError(Unauthorized 401) {Object} message - Error message
* 
* @apiError(Bad Request 400) {String} message - SES error message
*/
router.route('/:id/verifying')
  .patch(generateTokenVerifyEmail)


/**
* @api {PATCH} /api/v1/users/:id/verifying/:token
* @apiDescription Confirm verify email
* @apiGroup User
* @apiPermission Private
*
* @apiParam {String} id - userId
* @apiParam {String} token
*
* @apiSuccess (OK 200) {String} msg - Verify email successfully message
*
* @apiError(Unauthorized 401) {Object} message - Error message
* 
* @apiError(Bad Request 400) {Object} error - Token expire

* @apiError(Bad Request 400) {String} message - User not found or doesn't request to verify email
*/
router.route('/:id/verifying/:token')
  .get(confirmVerifyEmail)

export { router as userRoute }