import dotenv from 'dotenv';
dotenv.config();

import "@babel/polyfill";
import createError from 'http-errors';
import express from 'express';
import session from 'express-session';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import passport from 'passport';
import flash from 'connect-flash';


// Config
import { client } from "./configs/deepstream.config";
const db = require('./configs/sequelize.config');
require('./configs/cloudinary.config');
require('./configs/passport.config');
// Router
const indexRoute = require('./routes/index');

const app = express();

// Test connect DB
db.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

// DS login
client.login({ username: 'server', password: 'seRver' }, (success, data) => {
  if (success) {
    // start application and listen notification for case: project owner invite to project
    console.log('Deepstream connect state:', client.getConnectionState());
    // client.event.subscribe(`notifications/${req.user.userId}`, data =>{
    //   console.log(`You was invited to project: ${data}`);

    // })

    // client.getConnectionState() will now return 'OPEN'
  } else {
    // extra data can be optionaly sent from deepstream for
    // both successful and unsuccesful logins
    console.log(`already login`);

    // client.getConnectionState() will now return
    // 'AWAITING_AUTHENTICATION' or 'CLOSED'
    // if the maximum number of authentication
    // attempts has been exceeded.
  }
})


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


app.use('/', indexRoute);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
