import cloudinary from "cloudinary";

export function uploadPhoto(photo, id, folder) {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader
      .upload_stream({ public_id: id, folder: `TaskManagement/${folder}`, tags: 'user' }, (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result.url);
        }
      })
      .end(photo.buffer);
  });
}