import { project as projectModel } from "../models/Project";
import { taskGroup as taskGroupModel } from "../models/TaskGroup";
import { task as taskModel } from "../models/Task";

export async function getTaskAssociateProject(taskId,projectId) {
  try {

    projectModel.hasMany(taskGroupModel, {
      foreignKey: "projectId"
    });
    taskGroupModel.belongsTo(projectModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.hasMany(taskModel, {
      foreignKey: "taskGroupId"
    });
    taskModel.belongsTo(taskGroupModel, {
      foreignKey: "taskGroupId"
    });

    const task = await taskModel.findOne({
      where: {
        taskId
      },
      include: {
        model: taskGroupModel,
        required: true,
        include: {
          model: projectModel,
          required: true,
          where: {
            projectId
          }
        }
      }
    });

    return task;

  } catch (error) {
    console.log(error);

    return error;
  }
}
