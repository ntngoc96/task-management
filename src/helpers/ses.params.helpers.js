import { EMAIL_SRC } from "../configs/aws-ses.config";

export function generateParams(subjectData,body, email) {
  const params = {
    Destination: {
      ToAddresses: [email] // Email address/addresses that you want to send your email
    },
    Message: {
      Body: body,
      Subject: {
        Charset: "UTF-8",
        Data: subjectData
      }
    },
    Source: EMAIL_SRC
  };

  return params;
}