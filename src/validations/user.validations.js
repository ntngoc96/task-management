import { check } from "express-validator";

export const validateUserUpdateInfo = [
  check('fullName')
    .not()
    .isEmpty()
    .withMessage('fullName field is required')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('userName field must be at least characters 1 and below 200 characters'),

  check('city')
    .not()
    .isEmpty()
    .withMessage('City field is require')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('City field must be at least characters 1 and below 200 characters'),

  check('email')
    .isEmail()
    .withMessage('Email format wrong')
    .bail()
    .isLength({
      min:1,
      max: 100
    })
    .withMessage('Email field must be at least characters 1 and below 100 characters'),
]

export const validateUpdateAvatar = [
  check('avatar')
    .not()
    .isEmpty()
    .withMessage('Avatar cannot empty')
]