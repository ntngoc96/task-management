import { check } from "express-validator";

export const validateSignUp = [
  check('userName')
    .not()
    .isEmpty()
    .withMessage('userName field is required')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('userName field must be at least characters 1 and below 200 characters'),

  check('password')
    .not()
    .isEmpty()
    .withMessage('password field is require')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('Password field must be at least characters 1 and below 200 characters'),

  check('email')
    .isEmail()
    .withMessage('Email format wrong')
    .bail()
    .isLength({
      min:1,
      max: 100
    })
    .withMessage('Email field must be at least characters 1 and below 100 characters'),
]

export const validateSignIn = [
  check('userName')
    .not()
    .isEmpty()
    .withMessage('userName field is required')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('userName field must be at least characters 1 and below 200 characters'),

  check('password')
    .not()
    .isEmpty()
    .withMessage('password field is require')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('Password field must be at least characters 1 and below 200 characters'),

]
