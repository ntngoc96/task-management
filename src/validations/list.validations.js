import { check } from "express-validator";

export const validateTaskListCreate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('name field must be at least characters 1 and below 100 characters'),
  check('projectId')
    .not()
    .isEmpty()
    .withMessage('projectId field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('projectId field must be at least characters 1 and below 45 characters'),

]

export const validateTaskListUpdate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('name field must be at least characters 1 and below 100 characters'),
  check('position')
    .isInt({
      min: 1,
      max: 99
    })
    .withMessage('position field must be between 1 and 99'),

]