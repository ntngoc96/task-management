import { check } from "express-validator";

export const validateTaskCreate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('name field must be at least characters 1 and below 100 characters'),

]

export const validateMemberTaskAssign = [
  check('projectId')
    .not()
    .isEmpty()
    .withMessage('projectId field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('projectId field must be at least characters 1 and below 45 characters'),
  check('memberId')
    .not()
    .isEmpty()
    .withMessage('memberId field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('memberId field must be at least characters 1 and below 45 characters'),

]

export const validateTaskUpdate = [
  check('projectId')
    .not()
    .isEmpty()
    .withMessage('projectId field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('projectId field must be at least characters 1 and below 45 characters'),
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('name field must be at least characters 1 and below 100 characters'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('description field is require')
    .bail()
    .isLength({
      min: 1,
      max: 999
    })
    .withMessage('description field must be at least characters 1 and below 999 characters'),
  check('position')
    .not()
    .isEmpty()
    .withMessage('memberId field is require')
    .bail()
    .isLength({
      min: 1,
      max: 99
    })
    .withMessage('memberId field must be integer between 1 and 99'),


]

export const validateTaskUpdateDueDate = [
  check('dueDate')
    .not()
    .isEmpty()
    .withMessage('memberId field is require')
    .bail()
    .isISO8601()
    .withMessage('dueDate format wrong')
]