import { check } from "express-validator";

export const validateTaskGroupCreate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('name field must be at least characters 1 and below 45 characters'),

]
export const validateTaskGroupUpdate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('name field must be at least characters 1 and below 45 characters'),
  check('projectId')
    .not()
    .isEmpty()
    .withMessage('projectId field is require')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('projectId field must be at least characters 1 and below 45 characters'),
  check('position')
    .isInt({
      min: 1,
      max: 99
    })
    .withMessage('name field must be integer between 1 and 99'),

]