import { check } from "express-validator";

export const validateProjectCreate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('name field must be at least characters 1 and below 200 characters'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('description field is require')
    .bail()
    .isLength({
      min: 1,
      max: 999
    })
    .withMessage('description field must be at least characters 1 and below 999 characters'),
]

export const validateProjectUpdate = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('name field is require')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('name field must be at least characters 1 and below 200 characters'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('description field is require')
    .bail()
    .isLength({
      min: 1,
      max: 999
    })
    .withMessage('description field must be at least characters 1 and below 999 characters'),

]

export const validateProjectUpdateAvatar = [
  check('avatar')
    .not()
    .isEmpty()
    .withMessage('avatar field is require')
]