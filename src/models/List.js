import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const list = db.define("list",{
  listId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  taskId: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  position: {
    type: DataTypes.NUMBER
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
},{
  timestamps:false,
  freezeTableName: true, 
}) 
