import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const taskGroup = db.define("taskGroup",{
  taskGroupId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  projectId: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  position: {
    type: DataTypes.NUMBER
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
},{
  timestamps:false,
  freezeTableName: true, 
}) 
