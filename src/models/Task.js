import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const task = db.define("task",{
  taskId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  taskGroupId: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING
  },
  position: {
    type: DataTypes.NUMBER
  },
  dueDate: {
    type: DataTypes.DATE
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
},{
  timestamps:false,
  freezeTableName: true, 
}) 
