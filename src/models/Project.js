import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const project = db.define("project",{
  projectId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  projectLeaderId: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING
  },
  recent: {
    type: DataTypes.TINYINT
  },
  imageUrl: {
    type: DataTypes.STRING
  },
},{
  timestamps:false,
  freezeTableName: true, 
}) 
