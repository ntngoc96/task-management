import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const listItem = db.define("listItem",{
  listItemId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  listId: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  position: {
    type: DataTypes.NUMBER
  },
  state: {
    type: DataTypes.STRING
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
},{
  timestamps:false,
  freezeTableName: true, 
}) 
