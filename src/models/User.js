import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const userAccount = db.define("user",{
  userId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  userName: {
    type: DataTypes.STRING
  },
  fullName: {
    type: DataTypes.STRING
  },
  password: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  city: {
    type: DataTypes.STRING
  },
  avatar: {
    type: DataTypes.STRING
  },
  isVerified: {
    type: DataTypes.TINYINT
  },
  verifyEmailToken: {
    type: DataTypes.STRING
  },
  verifyEmailTokenExpire: {
    type: DataTypes.DATE
  },
},{
  timestamps:false,
  freezeTableName: true, 
}) 
