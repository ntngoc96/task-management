import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const projectMember = db.define("projectMember", {
  projectId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  userId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
}, {
  timestamps: false,
  freezeTableName: true,
}) 
