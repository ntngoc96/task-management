import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const notification = db.define("notification", {
  notificationId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  userId: {
    type: DataTypes.STRING,
  },
  projectId: {
    type: DataTypes.STRING,
  },
  activityId: {
    type: DataTypes.STRING,
  },
  seen: {
    type: DataTypes.TINYINT,
  },
}, {
  timestamps: false,
  freezeTableName: true,
}) 
