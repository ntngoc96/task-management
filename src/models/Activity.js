import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const activity = db.define("activity", {
  activityId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  userId: {
    type: DataTypes.STRING,
  },
  projectId: {
    type: DataTypes.STRING,
  },
  taskId: {
    type: DataTypes.STRING,
  },
  memberId: {
    type: DataTypes.STRING,
  },
  action: {
    type: DataTypes.STRING,
  },
  component:{
    type: DataTypes.STRING,
  },
  message:{
    type: DataTypes.STRING,
  },
  createdAt: {
    type: DataTypes.DATE,
  },
}, {
  timestamps: false,
  freezeTableName: true,
}) 
