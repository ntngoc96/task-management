import { DataTypes } from 'sequelize';
import db from '../configs/sequelize.config';

export const projectMemberTask = db.define("projectMemberTask", {
  projectId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  userId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  taskId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
}, {
  timestamps: false,
  freezeTableName: true,
}) 
