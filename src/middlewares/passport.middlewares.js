import httpStatus from "http-status";

export function isLoggedIn(req, res, next) {
  // If user loggedin let them go
  if (req.isAuthenticated())
    return next();
  // Unless back them to homepage
  res.status(httpStatus.UNAUTHORIZED).json({ msg: "Unauthorized" });
}