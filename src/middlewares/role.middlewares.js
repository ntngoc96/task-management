import httpStatus from "http-status";
import { projectMember as projectMemberModel } from "../models/ProjectMember";
import { project as projectModel } from "../models/Project";
import { taskGroup as taskGroupModel } from "../models/TaskGroup";
import { task as taskModel } from "../models/Task";
import { list as listModel } from "../models/List";
import { listItem as listItemModel } from "../models/ListItem";
import { projectMemberTask as projectMemberTaskModel } from "../models/ProjectMemberTask";

// middleware check user in group or not

export async function isMemberInTheProject(req, res, next) {
  try {
    const projectId = req.body.projectId || req.params.projectId || req.query.projectId;
    const { userId } = req.user;
    if (!projectId) {
      throw [{
        "msg": "projectId field is require",
        "param": "projectId",
      }]
    }

    req.projectId = projectId;
    const member = await projectMemberModel.findOne({
      where: {
        userId,
        projectId
      }
    });

    if (!member) {
      throw { msg: "user is not member of this project" }
    }

    next();

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}


export async function isProjectLeader(req, res, next) {
  try {
    const { userId } = req.user;
    const projectId = req.projectId || req.taskMembers.projectId;
    req.user.isLeader = false;

    const leader = await projectModel.findOne({
      where: {
        projectId,
        projectLeaderId: userId
      }
    });

    if (leader) {
      req.user.isLeader = true;
    }

    next();
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

// Member is project member task then they is also project member
export async function canManipulateTask(req, res, next) {
  try {
    const projectId = req.body.projectId || req.params.projectId || req.query.projectId;
    const taskId = req.body.taskId || req.params.taskId || req.query.taskId;
    const { userId } = req.user;
    if (!projectId) {
      throw [{
        "msg": "projectId field is require",
        "param": "projectId",
      }]
    }

    const memberInTask = await projectMemberTaskModel.findOne({
      where: {
        projectId,
        taskId,
        userId
      }, raw: true
    });

    if (!memberInTask) {
      throw { msg: "User doesn't have permission to view task" }
    }

    req.taskMembers = memberInTask;

    next();
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function isTaskOfTheProject(req, res, next) {
  try {
    const taskId = req.body.taskId || req.params.taskId || req.query.taskId;
    const projectId = req.body.projectId || req.params.projectId || req.query.projectId;

    projectModel.hasMany(taskGroupModel, {
      foreignKey: "projectId"
    });
    taskGroupModel.belongsTo(projectModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.hasMany(taskModel, {
      foreignKey: "taskGroupId"
    });
    taskModel.belongsTo(taskGroupModel, {
      foreignKey: "taskGroupId"
    });

    const task = await taskModel.findOne({
      where: {
        taskId
      },
      include: {
        model: taskGroupModel,
        required: true,
        include: {
          model: projectModel,
          required: true,
          where: {
            projectId
          }
        }
      }
    });

    req.childComponent = task;
    req.componentConvention = "Task";

    next();

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function isListItemOfTask(req, res, next) {
  try {
    const listItemId = req.body.listItemId || req.params.listItemId || req.query.listItemId;
    const { taskId } = req.taskMembers;
    taskModel.hasMany(listModel, {
      foreignKey: "taskId"
    });
    listModel.belongsTo(taskModel, {
      foreignKey: "taskId"
    });

    listModel.hasMany(listItemModel, {
      foreignKey: "listId"
    });
    listItemModel.belongsTo(listModel, {
      foreignKey: "listId"
    });

    const listItem = await listItemModel.findOne({
      where: {
        listItemId
      },
      include: [{
        model: listModel,
        required: true,
        include: [{
          model: taskModel,
          required: true,
          where: {
            taskId
          }
        }]
      }]
    });

    if (!listItem) {
      throw { msg: "List item is not belong to task" };
    }

    req.childComponent = listItem;

    next();
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function isListOfTask(req, res,next) {
  try {
    const listId = req.body.listId || req.params.listId || req.query.listId;
    const { taskId } = req.taskMembers;

    const list = await listModel.findOne({
      where: {
        listId,
        taskId
      }
    });

    if (!list) {
      throw { msg: "Task list is not belong to task" };
    }

    next();
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

//expire
export async function isListOfTheProject(req, res, next) {
  try {
    const listId = req.body.listId || req.params.listId || req.query.listId;
    const projectId = req.body.projectId || req.params.projectId || req.query.projectId;

    projectModel.hasMany(taskGroupModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.belongsTo(projectModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.hasMany(taskModel, {
      foreignKey: "taskGroupId"
    });

    taskModel.belongsTo(taskGroupModel, {
      foreignKey: "taskGroupId"
    });

    taskModel.hasMany(listModel, {
      foreignKey: "taskId"
    });
    listModel.belongsTo(taskModel, {
      foreignKey: "taskId"
    });

    const list = await listModel.findOne({
      where: {
        listId
      },
      include: [{
        model: taskModel,
        required: true,
        include: [{
          model: taskGroupModel,
          required: true,
          include: [{
            model: projectModel,
            required: true,
            where: {
              projectId
            }
          }]
        }]
      }]
    })

    req.childComponent = list;
    req.componentConvention = "Task list";

    next();

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

//expire 
export async function isListItemOfTheProject(req, res, next) {
  try {
    const listItemId = req.body.listItemId || req.params.listItemId || req.query.listItemId;
    const projectId = req.body.projectId || req.params.projectId || req.query.projectId;

    projectModel.hasMany(taskGroupModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.belongsTo(projectModel, {
      foreignKey: "projectId"
    });

    taskGroupModel.hasMany(taskModel, {
      foreignKey: "taskGroupId"
    });

    taskModel.belongsTo(taskGroupModel, {
      foreignKey: "taskGroupId"
    });

    taskModel.hasMany(listModel, {
      foreignKey: "taskId"
    });
    listModel.belongsTo(taskModel, {
      foreignKey: "taskId"
    });

    listModel.hasMany(listItemModel, {
      foreignKey: "listId"
    });
    listItemModel.belongsTo(listModel, {
      foreignKey: "listId"
    });

    const listItem = await listItemModel.findOne({
      where: {
        listItemId
      },
      include: [{
        model: listModel,
        required: true,
        include: [{
          model: taskModel,
          required: true,
          include: [{
            model: taskGroupModel,
            required: true,
            include: [{
              model: projectModel,
              required: true,
              where: {
                projectId
              }
            }]
          }]
        }]
      }]
    })

    req.childComponent = listItem;
    req.componentConvention = "Task list item";

    next();

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}

export async function handleCheckComponentOfTheProject(req, res, next) {
  try {
    const { childComponent, componentConvention } = req;


    // if childComponent not null then it is in project and vice versa
    if (!childComponent) {
      throw { msg: `${componentConvention} is not in project` };
    }

    next();

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json(error)
  }
}