import { validationResult } from "express-validator";
import httpStatus from "http-status";

export function handleValidationError(req, res, next) {
  //handle validate error
  const result = validationResult(req);
  const hasErrors = !result.isEmpty();
  if (hasErrors) {
    res.status(httpStatus.BAD_REQUEST).json(result.errors);
  } else {
    next();
  }

}